 var mongoose = require("mongoose");
 mongoose.connect("mongodb://localhost:27017/InstaDeli");
 
 var db = mongoose.connection;
 
 db.on("error", console.error.bind(console, "connection error"));
 db.once("open", function(callback) {
     console.log("Connection succeeded.");
 });
 
 var Schema = mongoose.Schema;
 
 const productSchema = mongoose.Schema({
    id: { type: Number},
    name: { type: String, required: true},
    images: { type: Array, required: true},
    oldPrice: Number,
    newPrice: { type : Number, required: true},
    discount: Number,
    ratingsCount: Number,
    ratingsValue: Number,
    description: String,
    availibilityCount: Number,
    color: Array,
    size: Array,
    weight: Number,
    categoryId: Number

});
 
 var Product = mongoose.model("Product", productSchema);
 
 var product = new Product(
    
    { 
        "id": 2,
        "name": "PC All-in-One",
        "images": [
            {
                "small": "../src/assets/images/products/pc/1-small.png",
                "medium": "../src/assets/images/products/pc/1-medium.png",
                "big": "../src/assets/images/products/pc/1-big.png"
            },
            {
                "small": "../src/assets/images/products/pc/2-small.png",
                "medium": "../src/assets/images/products/pc/2-medium.png",
                "big": "../src/assets/images/products/pc/2-big.png"
            },
            {
                "small": "../src/assets/images/products/pc/3-small.png",
                "medium": "../src/assets/images/products/pc/3-medium.png",
                "big": "../src/assets/images/products/pc/3-big.png"
            },
            {
                "small": "../src/assets/images/products/pc/4-small.png",
                "medium": "../src/assets/images/products/pc/4-medium.png",
                "big": "../src/assets/images/products/pc/4-big.png"
            },
            {
                "small": "../src/assets/images/products/pc/5-small.png",
                "medium": "../src/assets/images/products/pc/5-medium.png",
                "big": "../src/assets/images/products/pc/5-big.png"
            },
            {
                "small": "../src/assets/images/products/pc/6-small.png",
                "medium": "../src/assets/images/products/pc/6-medium.png",
                "big": "../src/assets/images/products/pc/6-big.png"
            }
        ],
        "oldPrice": 2510,
        "newPrice": 2179.99,
        "discount": 15,
        "ratingsCount": 8,
        "ratingsValue": 800,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut congue eleifend nulla vel rutrum. Donec tempus metus non erat vehicula, vel hendrerit sem interdum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.",
        "availibilityCount": 3,
        "color": ["#1D1D1D","#DADADA","#597087"],
        "size": ["24''","28''","32''"],
        "weight": 9550,
        "categoryId": 100
    }
    
 );
 
 product.save(function(error) {
     console.log("Your bee has been saved!");
 if (error) {
     console.error(error);
  }
 });