const path = require("path");
const express = require("express");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/familymart", { useNewUrlParser: true })
    .then(() => {
        console.log("connected to the database");
    })
    .catch(() => {
        console.log("connection failed");
    });

// Schemas imports
const mainCarouselSlide = require('./models/mainCarousel');
const homeBanner = require('./models/homeBanners');
const product = require('./models/product')


const app = express();
app.use("/", express.static(path.join(__dirname, "angular")));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, DELETE, OPTIONS"
    );
    next();
  });

app.get('/slides',(req, res, next) => {
    mainCarouselSlide.find().then(documents => {
        res.status(200).json({
            message:"atta boy",
            slides: documents
        });
    });
});

app.get('/homeBanners', (req, res, next) => {
    homeBanner.find().then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            banners: documents
        });
    });
    
});

// to get all products
app.get('/products', (req, res, next) => {
    product.find().then(documents => {
        console.log(documents);
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
    
});

// to get a product by id

app.get('/products/:id', (req, res, next) => {
    product.findById(req.params.id).then(document => {
        res.status(200).json({
            message: 'successfully got it',
            product : document
        });
    });
});

// to get products by category

app.get('/categories/:name', (req, res, next) => {
    let name = req.params.name;
    // .replace(/%20/g, ' ')
    product.find({categoryName: name}).then(documents => {
        console.log(documents);
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the produces

app.get('/produces', (req, res, next) => {
    product.find({categoryName:'produces'}). then(documents => {
        console.log(documents);
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the bakery and desserts

app.get('/bakery', (req, res, next) => {
    product.find({categoryName: 'bakery & desserts'}).then(documents => {
        res.status(200).json({
            message: 'successfully gor it',
            products: documents
        });
    });
});

// to get meat and fish

app.get('/meatFish', (req, res, next) => {
    product.find({categoryName: 'meat & fish'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the breakfast

app.get('/breakfast', (req, res, next) => {
    product.find({categoryName: 'breakfast'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the coffe, tea and sweeteners

app.get('/coffeeTeaSweeteners', (req, res, next) => {
    product.find({categoryName: 'coffee, tea & sweeteners'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the snacks, candy and nuts

app.get('/snacksCandyNuts', (req, res, next) => {
    product.find({categoryName: 'snacks, candy & nuts'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the beverages

app.get('/beverages', (req, res, next) => {
    product.find({categoryName: 'beverages'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the grains, Pasta and rice

app.get('/grains', (req, res, next) => {
    product.find({categoryName: 'grains, pasta & rice'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to the the spices and seasonings

app.get('/spices', (req, res, next) => {
    product.find({categoryName: 'spices & seasonings'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the dairy and eggs

app.get('/dairy', (req, res, next) => {
    product.find({categoryName: 'dairy & eggs'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get paper products and food storage

app.get('/paper', (req, res, next) => {
    product.find({categoryName: 'paper products & food storage'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the babies products

app.get('/babies', (req, res, next) => {
    product.find({categoryName: 'babies'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the personal care
app.get('/personal', (req, res, next) => {
    product.find({categoryName: 'personal care & hygiene'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the cleaning and laundry

app.get('/cleaning', (req, res, next) => {
    product.find({categoryName: 'cleaning & laundry'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the electronics and utensils

app.get('/electronics', (req, res, next) => {
    product.find({categoryName: 'electronics & utensils'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the featured products
app.get('/featured', (req, res, next) => {
    product.find({label: 'featured'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the onSale products
app.get('/onSale', (req, res, next) => {
    product.find({label: 'onSale'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the top rated products
app.get('/topRated', (req, res, next) => {
    product.find({label: 'topRated'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

// to get the newarrival products
app.get('/newArrival', (req, res, next) => {
    product.find({label: 'newArrival'}).then(documents => {
        res.status(200).json({
            message: 'successfully got it',
            products: documents
        });
    });
});

app.use((req, res, next) => {
    res.sendFile(path.join(__dirname, "angular", "index.html"));
});










module.exports = app;