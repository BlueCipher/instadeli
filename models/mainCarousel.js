const mongoose = require('mongoose');

const slideSchema = mongoose.Schema({
    title: { type: String, required: true},
    subtitle: { type: String, required: true},
    image: { type: String, required: true}
});

module.exports = mongoose.model('MainCarouselSlide', slideSchema);