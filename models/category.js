const mongoose = require('mongoose');

const categorySchema = mongoose.Schema({
    id: { type: number, required: true},
    name: { type: String, required: true},
    image: { type: String, required: true},
    hasSubCategory: { type: Boolean, required: true},
    parentId: { type: number, required: true}

});

module.exports = mongoose.model('HomeBanners', bannerSchema);