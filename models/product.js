const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    id: { type: Number, required: true},
    name: { type: String, required: true},
    images: { type: Array, required: true},
    oldPrice: Number,
    newPrice: { type : Number, required: true},
    discount: Number,
    ratingsCount: Number,
    ratingsValue: Number,
    description: String,
    availibilityCount: Number,
    weight: Number,
    categoryId: Number,
    parentId: Number

});

module.exports = mongoose.model('Product', productSchema);
